import React from 'react';
import NumberFormat from 'react-number-format';

import './App.css';

function App() {
  return (
    <div className="App">
      Enter You Data
      <br/> 
      <NumberFormat thousandSeparator={true} maxLength={4} />

    </div>
  );
}

export default App;
